/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of JText
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package jtext;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainFrame extends JFrame implements ChangeListener {
    private static MainFrame instance;
    private JTabbedPane tabs;
    private ButtonsRow buttonsRow;
    private ArrayList<TextPane> textPanes;
    private JMenuItem create, open, save, saveAs, saveAll, closeTab, closeAllTabs, exit, selectAll, copy, cut, paste, help, license;

    private MainFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        position(700, 600);
        try {
            setIconImage(ImageIO.read(this.getClass().getResource("/icons/gnu.png")));
        } catch (IOException e) {
            System.err.println("Error while reading favicon");
            e.printStackTrace();
        }

        textPanes = new ArrayList<TextPane>();

        // Menu
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        JMenu helpMenu = new JMenu("Help");
        create = new JMenuItem(new ButtonAction(ButtonType.NEW, this, KeyStroke.getKeyStroke("ctrl N")));
        open = new JMenuItem(new ButtonAction(ButtonType.OPEN, this, KeyStroke.getKeyStroke("ctrl O")));
        save = new JMenuItem(new ButtonAction(ButtonType.SAVE, this, KeyStroke.getKeyStroke("ctrl S")));
        saveAs = new JMenuItem(new ButtonAction(ButtonType.SAVE_ASS, this, KeyStroke.getKeyStroke("ctrl shift S")));
        saveAll = new JMenuItem(new ButtonAction(ButtonType.SAVE_ALL, this, KeyStroke.getKeyStroke("ctrl L")));
        closeTab = new JMenuItem(new ButtonAction(ButtonType.CLOSE_TAB, this, KeyStroke.getKeyStroke("ctrl F4")));
        closeAllTabs = new JMenuItem(new ButtonAction(ButtonType.CLOSE_ALL_TABS, true, this));
        exit = new JMenuItem(new ButtonAction(ButtonType.EXIT, true, this));
        selectAll = new JMenuItem(new ButtonAction(ButtonType.SELECT_ALL, this, KeyStroke.getKeyStroke("ctrl A")));
        copy = new JMenuItem(new ButtonAction(ButtonType.COPY, this, KeyStroke.getKeyStroke("ctrl C")));
        cut = new JMenuItem(new ButtonAction(ButtonType.CUT, this, KeyStroke.getKeyStroke("ctrl X")));
        paste = new JMenuItem(new ButtonAction(ButtonType.PASTE, this, KeyStroke.getKeyStroke("ctrl V")));
        help = new JMenuItem(new ButtonAction(ButtonType.HELP, true, this));
        license = new JMenuItem(new ButtonAction(ButtonType.LICENSE, true, this));
        fileMenu.add(create);
        fileMenu.add(open);
        fileMenu.add(save);
        fileMenu.add(saveAs);
        fileMenu.add(saveAll);
        fileMenu.addSeparator();
        fileMenu.add(closeTab);
        fileMenu.add(closeAllTabs);
        fileMenu.add(exit);
        editMenu.add(selectAll);
        editMenu.addSeparator();
        editMenu.add(copy);
        editMenu.add(cut);
        editMenu.add(paste);
        helpMenu.add(help);
        helpMenu.add(license);
        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(helpMenu);
        setJMenuBar(menuBar);

        buttonsRow = new ButtonsRow(this);
        tabs = new JTabbedPane(JTabbedPane.TOP);
        tabs.addChangeListener(buttonsRow);
        tabs.addChangeListener(this);
        stateChanged(null);

        setLayout(new BorderLayout(0, 5));
        add(buttonsRow, BorderLayout.NORTH);
        add(tabs, BorderLayout.CENTER);
    }

    /**
     * Sets the frame's size with centered position
     *
     * @param sizeX width
     * @param sizeY height
     */
    private void position(int sizeX, int sizeY) {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int marginX, marginY;

        marginX = (screen.width - sizeX) / 2;
        marginY = (screen.height - sizeY) / 2;
        setBounds(marginX, marginY, sizeX, sizeY);
    }

    public ArrayList<TextPane> getTextPanes() {
        return textPanes;
    }

    /**
     * Check if the file is already opened
     * @param file File to check
     * @return true if it's opened
     */
    private boolean isOpened(File file) {
        for (TextPane textPane : textPanes) {
            if (textPane.getFile().equals(file))
                return true;
        }
        return false;
    }

    /**
     * @return The selected TextPane in tabs
     */
    private TextPane getSelectedPane() {
        return ((TextPane)tabs.getSelectedComponent());
    }

    /**
     * Open a file that already exists
     * @param file File to open
     */
    public void openFile(File file) {
        if (isOpened(file)) {
            // Select the tab that has the file open
            tabs.setSelectedIndex(tabs.indexOfTab(file.getName()));
        } else {
            TextPane newTextPane = new TextPane(file);
            if (newTextPane.loadText()) {
                textPanes.add(newTextPane);
                tabs.addTab(file.getName(), newTextPane);
                tabs.setSelectedIndex(tabs.indexOfTab(file.getName()));
                if (!file.canWrite())
                    JOptionPane.showMessageDialog(this, "This file is read-only",
                            "Warning", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     * Create a buffer for a file that doesn't exist. If the file exists, it will be opened
     * @param file New file
     */
    public void newFile(File file) {
        if (file.exists()) {
            openFile(file);
        } else if (isOpened(file)) {
            // Select the tab that has the file open
            tabs.setSelectedIndex(tabs.indexOfTab(file.getName()));
        } else {
            TextPane newTextPane = new TextPane(file);
            textPanes.add(newTextPane);
            tabs.addTab(file.getName(), newTextPane);
        }
    }

    /**
     * Save the file in current tab
     */
    public void saveFile() {
        getSelectedPane().saveText();
    }

    /**
     * Save as the file in current tab
     * @param file File to save in
     */
    public void saveAsFile(File file) {
        getSelectedPane().saveText(file);
    }

    /**
     * Save all files opened
     */
    public void saveAllFiles() {
        for (TextPane textPane : textPanes)
            textPane.saveText();
    }

    /**
     * Close current tab. And it does it RIGHT NOW. WITHOUT ASKING. BECAUSE IT CAN.
     */
    public void closeTabNow() {
        textPanes.remove(getSelectedPane());
        tabs.remove(tabs.getSelectedComponent());
    }

    /**
     * Close all tabs. And it does it RIGHT NOW. WITHOUT ASKING. BECAUSE IT CAN.
     */
    public void closeAllTabsNow() {
        textPanes.clear();
        tabs.removeAll();
    }

    /**
     * Select all text in current tab
     */
    public void selectAllText() {
        getSelectedPane().getTextArea().selectAll();
    }

    /**
     * Copy selected text in current tab
     */
    public void copyText() {
        getSelectedPane().getTextArea().copy();
    }

    /**
     * Cut selected text in current tab
     */
    public void cutText() {
        getSelectedPane().getTextArea().cut();
    }

    /**
     * Paste text in current tab
     */
    public void pasteText() {
        getSelectedPane().getTextArea().paste();
    }

    /**
     * Open a tab with the license text (non editable)
     */
    public void showLicense() {
        File licenseFile = new File(this.getClass().getResource("/LICENSE").toExternalForm());
        TextPane licensePane = new TextPane(licenseFile);
        try {
            licensePane.getTextArea().read(new InputStreamReader(getClass().getResourceAsStream("/LICENSE")), licenseFile.getName());
            licensePane.getTextArea().setEditable(false);
            textPanes.add(licensePane);
            tabs.addTab("License", licensePane);
            tabs.setSelectedComponent(licensePane);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error while loading license","Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        boolean active = !textPanes.isEmpty();
        save.setEnabled(active);
        saveAs.setEnabled(active);
        saveAll.setEnabled(active);
        copy.setEnabled(active);
        cut.setEnabled(active);
        paste.setEnabled(active);
        closeTab.setEnabled(active);
        closeAllTabs.setEnabled(active);
        selectAll.setEnabled(active);

        if (active) {
            setTitle(getSelectedPane().getFile().getName());
        } else {
            setTitle("JText");
        }
    }

    /**
     * Return an instance of the MainFrame
     * @return the Mainframe
     */
    public static MainFrame getInstance() {
        if (instance == null)
            instance = new MainFrame();
        return instance;
    }
}
