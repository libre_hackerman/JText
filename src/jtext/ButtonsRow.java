/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of JText
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package jtext;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class ButtonsRow extends Box implements ChangeListener {
    private static final Dimension BUTTONS_SIZE = new Dimension(40, 40);

    private MainFrame mainFrame;
    private JButton newFile, open, save, saveAs, saveAll, copy, cut, paste, exit;

    public ButtonsRow(MainFrame mainFrame) {
        super(BoxLayout.X_AXIS);
        this.mainFrame = mainFrame;

        newFile = new JButton(new ButtonAction(ButtonType.NEW, false, mainFrame));
        open = new JButton(new ButtonAction(ButtonType.OPEN, false, mainFrame));
        save = new JButton(new ButtonAction(ButtonType.SAVE, false, mainFrame));
        saveAs = new JButton(new ButtonAction(ButtonType.SAVE_ASS, false, mainFrame));
        saveAll = new JButton(new ButtonAction(ButtonType.SAVE_ALL, false, mainFrame));
        exit = new JButton(new ButtonAction(ButtonType.EXIT, false, mainFrame));
        copy = new JButton(new ButtonAction(ButtonType.COPY, false, mainFrame));
        cut = new JButton(new ButtonAction(ButtonType.CUT, false, mainFrame));
        paste = new JButton(new ButtonAction(ButtonType.PASTE, false, mainFrame));

        newFile.setMaximumSize(BUTTONS_SIZE);
        open.setMaximumSize(BUTTONS_SIZE);
        save.setMaximumSize(BUTTONS_SIZE);
        saveAs.setMaximumSize(BUTTONS_SIZE);
        saveAll.setMaximumSize(BUTTONS_SIZE);
        exit.setMaximumSize(BUTTONS_SIZE);
        copy.setMaximumSize(BUTTONS_SIZE);
        cut.setMaximumSize(BUTTONS_SIZE);
        paste.setMaximumSize(BUTTONS_SIZE);

        add(newFile);
        add(Box.createHorizontalStrut(3));
        add(open);
        add(Box.createHorizontalStrut(3));
        add(save);
        add(Box.createHorizontalStrut(3));
        add(saveAs);
        add(Box.createHorizontalStrut(3));
        add(saveAll);
        add(Box.createHorizontalStrut(3));
        add(copy);
        add(Box.createHorizontalStrut(3));
        add(cut);
        add(Box.createHorizontalStrut(3));
        add(paste);
        add(Box.createHorizontalStrut(3));
        add(exit);

        stateChanged(null);
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        boolean active = !mainFrame.getTextPanes().isEmpty();
        save.setEnabled(active);
        saveAs.setEnabled(active);
        saveAll.setEnabled(active);
        copy.setEnabled(active);
        cut.setEnabled(active);
        paste.setEnabled(active);
    }

}
