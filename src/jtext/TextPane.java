/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of JText
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package jtext;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class TextPane extends JPanel {
    private File file;
    private JTextArea jTextArea;

    public TextPane(File file) {
        this.file = file;
        jTextArea = new JTextArea();
        setLayout(new BorderLayout());
        add(new JScrollPane(jTextArea), BorderLayout.CENTER);
    }

    public JTextArea getTextArea() {
        return jTextArea;
    }

    public File getFile() {
        return file;
    }

    /**
     * Loads the text from default file into jTextArea
     * @return success
     */
    public boolean loadText() {
        try {
            jTextArea.read(new FileReader(file), file.getName());
            return true;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Error when reading " + file.getAbsolutePath(),
                    "Reading error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    /**
     * Writes the text from jTextArea into default file
     * @return success
     */
    public boolean saveText() {
        try {
            jTextArea.write(new FileWriter(file));
            return true;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Error when writing to " + file.getAbsolutePath(),
                    "Writing error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    /**
     * Writes the text from jTextArea into especified file
     * @param file File in which to write
     * @return success
     */
    public boolean saveText(File file) {
        try {
            jTextArea.write(new BufferedWriter(new FileWriter(file)));
            return true;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Error when writing to " + file.getAbsolutePath(),
                    "Writing error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

}
