/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of JText
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package jtext;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

public class ButtonAction extends AbstractAction {
    private MainFrame mainFrame;

    public ButtonAction(ButtonType buttonType, boolean menu, MainFrame mainFrame) {
        this.mainFrame = mainFrame;

        putValue("ButtonType", buttonType);
        if (menu)
            putValue(Action.NAME, buttonType.getName());
        putValue(Action.SHORT_DESCRIPTION, buttonType.getDescription());

        if (menu) { // Icons for menus
            if (buttonType.getMenuIconPath() != null)
                putValue(Action.SMALL_ICON, new ImageIcon(this.getClass().getResource(buttonType.getMenuIconPath())));
        } else { // Icons for buttons
            if (buttonType == ButtonType.EXIT)
                putValue(Action.SMALL_ICON, UIManager.getIcon("OptionPane.errorIcon"));
            else if (buttonType.getMenuIconPath() != null)
                putValue(Action.SMALL_ICON, new ImageIcon(this.getClass().getResource(buttonType.getButtonIconPath())));
        }
    }

    public ButtonAction(ButtonType buttonType, MainFrame mainFrame, KeyStroke accelerator) {
        this(buttonType, true, mainFrame);
        putValue(Action.ACCELERATOR_KEY, accelerator);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        JFileChooser explorer;
        File file;

        switch ((ButtonType)getValue("ButtonType")) {
            case NEW:
                explorer = new JFileChooser();
                explorer.setFileSelectionMode(JFileChooser.FILES_ONLY);

                if (explorer.showDialog(mainFrame, "New") == JFileChooser.APPROVE_OPTION) {
                    file = explorer.getSelectedFile();
                    mainFrame.newFile(file);
                }
                break;
            case OPEN:
                explorer = new JFileChooser();
                explorer.setFileSelectionMode(JFileChooser.FILES_ONLY);
                explorer.setMultiSelectionEnabled(true);

                if (explorer.showDialog(mainFrame, "Open") == JFileChooser.APPROVE_OPTION) {
                    for (File f : explorer.getSelectedFiles())
                        mainFrame.openFile(f);

                }
                break;
            case SAVE:
                mainFrame.saveFile();
                break;
            case SAVE_ASS:
                explorer = new JFileChooser();
                explorer.setFileSelectionMode(JFileChooser.FILES_ONLY);

                if (explorer.showDialog(mainFrame, "Save As") == JFileChooser.APPROVE_OPTION) {
                    mainFrame.saveAsFile(explorer.getSelectedFile());
                }
                break;
            case SAVE_ALL:
                mainFrame.saveAllFiles();
                break;
            case EXIT:
                System.exit(0);
                break;
            case SELECT_ALL:
                mainFrame.selectAllText();
                break;
            case COPY:
                mainFrame.copyText();
                break;
            case CUT:
                mainFrame.cutText();
                break;
            case PASTE:
                mainFrame.pasteText();
                break;
            case CLOSE_TAB:
                mainFrame.closeTabNow();
                break;
            case CLOSE_ALL_TABS:
                mainFrame.closeAllTabsNow();
                break;
            case HELP:
                JOptionPane.showMessageDialog(mainFrame, "Did you see this program? You don't " +
                        "need help with it, and you know","Really?", JOptionPane.OK_OPTION);
                break;
            case LICENSE:
                mainFrame.showLicense();
                break;
            default:
                System.err.println("Unknown action");
        }
    }
}

enum ButtonType {

    NEW("New", "New file...", "/icons/New24.gif", "/icons/New16.gif"),
    OPEN("Open", "Open file...", "/icons/Open24.gif", "/icons/Open16.gif"),
    SAVE("Save", "Save file...", "/icons/Save24.gif", "/icons/Save16.gif"),
    SAVE_ASS("Save As", "Save as file...", "/icons/SaveAs24.gif", "/icons/SaveAs16.gif"),
    SAVE_ALL("Save All", "Save all files", "/icons/SaveAll24.gif", "/icons/SaveAll16.gif"),
    EXIT("Exit", "Exit JText", null, null),
    SELECT_ALL("Select All","Select all text in current tab", null, null),
    COPY("Copy", "Copy selected text", "/icons/Copy24.gif", "/icons/Copy16.gif"),
    CUT("Cut", "Cut selected text", "/icons/Cut24.gif", "/icons/Cut16.gif"),
    PASTE("Paste", "Paste clipboard text", "/icons/Paste24.gif", "/icons/Paste16.gif"),
    CLOSE_TAB("Close tab", "Close current tab", null, null),
    CLOSE_ALL_TABS("Close all tabs", "Close all tabs", null, null),
    HELP("Help", "Open help dialog", null, null),
    LICENSE("License", "Display license", null, null);

    private String name, description, menuIconPath, buttonIconPath;

    ButtonType(String name, String description,String buttonIconPath, String menuIconPath) {
        this.name = name;
        this.description = description;
        this.buttonIconPath = buttonIconPath;
        this.menuIconPath = menuIconPath;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getButtonIconPath() {
        return buttonIconPath;
    }

    public String getMenuIconPath() {
        return menuIconPath;
    }
}
